//
//  NetworkManager.swift
//  CricketDemo
//
//  Created by INDRESH on 04/04/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import Foundation


public enum APIServiceError: Error {
    case apiError
    case badURL
    case invalidResponse
    case noData
    case decodeError
}

class APIManager {
    func fetchCoursesJSON(from urlString: String, completion: @escaping (Result<TeamsDetails, APIServiceError>) -> ()) {
        
        guard let url = URL(string: urlString) else {
            completion(.failure(.badURL))
            return }
        
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            if err != nil {
                completion(.failure(.apiError))
                return
            }
            
            guard let data = data else {
                completion(.failure(.noData))
                return
            }
            do {
                let courses = try JSONDecoder().decode(TeamsDetails.self, from: data)
                completion(.success(courses))
                
                
            } catch let Jsonerror {
                print(Jsonerror.localizedDescription)
                completion(.failure(.decodeError))
                
            }
            
            
        }.resume()
    }
}
