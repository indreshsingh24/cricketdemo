//
//  Players.swift
//  CricketDemo
//
//  Created by INDRESH on 04/04/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import Foundation

class TeamList {
    let Name_Full: String
    let Name_Short: String
    let playesName: [String: PlayerName]
    
    init(Name_Full: String, Name_Short: String, playesName: [String: PlayerName]) {
        self.Name_Full = Name_Full
        self.Name_Short = Name_Short
        self.playesName = playesName
    }
}
