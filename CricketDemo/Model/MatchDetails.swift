//
//  MatchDetails.swift
//  CricketDemo
//
//  Created by INDRESH on 04/04/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import Foundation


//struct MatchDetails: Decodable {
//    var Matchdetail: TeamDetails
//}
//
//struct TeamDetails: Decodable {
//    var Team_Home: String
//    var Team_Away: String
//}


struct TeamsDetails: Decodable {
    var Teams: [String: Team]
}

struct Team: Decodable {
    var Name_Full: String
    var Name_Short: String
     var Players: [String: PlayerName]
}

struct PlayerName: Decodable {
    var Name_Full: String
    var Iscaptain: Bool?
    var Iskeeper: Bool?
}
