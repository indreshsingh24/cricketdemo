//
//  PlayerListCell.swift
//  CricketDemo
//
//  Created by INDRESH on 04/04/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import UIKit

class PlayerListCell: UITableViewCell {

    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var captionImage: UIImageView!
     @IBOutlet weak var wicketKeeperImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
