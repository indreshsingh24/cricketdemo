//
//  ViewController.swift
//  CricketDemo
//
//  Created by INDRESH on 04/04/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var teamList = [TeamList]()
    var tableValue = [PlayerName]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APIManager().fetchCoursesJSON(from: "https://cricket.yahoo.net/sifeeds/cricket/live/json/sapk01222019186652.json") { (result) in
            switch result {
            case .success(let matchDetails):
                for data in  matchDetails.Teams {
                    self.teamList.append(TeamList.init(Name_Full:  data.value.Name_Full, Name_Short:  data.value.Name_Short, playesName:  data.value.Players))
                    
                }
                DispatchQueue.main.async {
                    for(_, value) in self.teamList.first!.playesName {
                        self.tableValue.append(value)
                        self.tableView.reloadData()
                    }
                    
                    self.segment.setTitle(self.teamList.first?.Name_Full, forSegmentAt: 0)
                    self.segment.setTitle(self.teamList.last?.Name_Full, forSegmentAt: 1)
                    print(matchDetails)
                }
                
            case .failure(_):
                print("Failed")
            }
            
        }
    }

    @IBAction func segmentButtonClicked(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            tableValue.removeAll()
           for(_, value) in self.teamList.first!.playesName {
                self.tableValue.append(value)
            }
            tableView.reloadData()
        }else{
            tableValue.removeAll()
            for(_, value) in self.teamList.last!.playesName {
                self.tableValue.append(value)
            }
            tableView.reloadData()
        }
    }
    
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerListCell", for: indexPath) as! PlayerListCell
        
        cell.playerName.text = "\(tableValue[indexPath.row].Name_Full)"
        cell.captionImage.isHidden = tableValue[indexPath.row].Iscaptain ?? false ? false: true
        cell.wicketKeeperImage.isHidden = tableValue[indexPath.row].Iskeeper ?? false ? false: true
        return cell
    }
}

